import paho.mqtt.publish as publish
import smtplib, ssl
import secrets
import pandas as pd
import datetime
import json
from influxdb import DataFrameClient
dbclient = DataFrameClient(host='nangluong.iotmind.vn', port=8086, username='root', password='root', database='SCG')
dbclient.create_database('SCG')

def SendCommand(NewSetPoint):
    command = {
        "w":[
            {
                "tag":"PLC1:tld_c1",
                "value":NewSetPoint["sprClinkerVCM"]
            },
            {
                "tag":"PLC1:tld_c2",
                "value":NewSetPoint["sprClinkerSG1"]
            },
            {
                "tag":"PLC1:tld_c3",
                "value":NewSetPoint["sprClinkerSG2"]
            },
            {
                "tag":"PLC1:tld_pg2",
                "value":NewSetPoint["sprGypsum1"]
            },
            {
                "tag":"PLC1:tld_pg3",
                "value":NewSetPoint["sprNDBlackstone"]
            },
            {
                "tag":"PLC1:tld_trobay",
                "value":NewSetPoint["sprFlyAsh"]
            },
            {
                "tag":"PLC1:tld_thachcao",
                "value":NewSetPoint["sprGypsum2"]
            },
            {
                "tag":"PLC3:tld_pgm",
                "value":NewSetPoint["sprMapei"]
            },
            {
                "tag":"PLC1:nsd_all",
                "value":NewSetPoint["spcTotal"]
            },
        ]
    }
    return publish.single(topic="test", payload=str(command), qos=2, hostname="nangluong.iotmind.vn", port=16766, auth={'username':"mind", 'password':"123"})
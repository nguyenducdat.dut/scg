import paho.mqtt.publish as publish
import smtplib, ssl
import secrets
import pandas as pd
import datetime
import json
from influxdb import DataFrameClient
dbclient = DataFrameClient(host='nangluong.iotmind.vn', port=8086, username='root', password='root', database='SCG')
dbclient.create_database('SCG')

def SendCommitKey(receiver_email, CommitKey=""):
    port = 465
    smtp_server = "smtp.gmail.com"
    sender_email = "scgxmsg@gmail.com"
    password = "xmsg@iot"

    message = """Subject: XMSG commit key\n\n{}""".format(CommitKey)
    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
        server.login(sender_email, password)
        server.sendmail(sender_email, receiver_email, message)
    return print("Done")

def CheckEmailAndGenerateCommitKey(Email): 
    EmailsList = dbclient.query('select * from EmailsList')["EmailsList"]
    if len(EmailsList[EmailsList.email.isin([Email])]) == 1:
        CommitKey = secrets.token_hex(16)
        time = datetime.datetime.utcnow()
        df = pd.DataFrame([{"time":time, "email":Email, "CommitKey":CommitKey}]).set_index('time')
        dbclient.write_points(df, 'CommitKey', field_columns=['CommitKey'])
        SendCommitKey(Email, CommitKey)
        result = "The commit key has been sent to your email and expires after 1 minute"
    else:
        result = "Email is not registered"
    return result
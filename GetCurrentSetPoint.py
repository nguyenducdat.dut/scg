import paho.mqtt.publish as publish
import smtplib, ssl
import secrets
import pandas as pd
import datetime
import json
from influxdb import DataFrameClient
dbclient = DataFrameClient(host='nangluong.iotmind.vn', port=8086, username='root', password='root', database='SCG')
dbclient.create_database('SCG')

def GetCurrentSetPoint():
    df = pd.DataFrame()
    try:
        df = dbclient.query('select * \
                                from realtime \
                                where parameter =~ /sp/ AND time >= now() - 2s')['realtime']
        df = df[df.index == df.index.max()] 
        sprClinkerVCM = df[(df['parameter'] == 'spr') & (df['device'] == 'Clinker VCM')]['value'].values[0]
        sprClinkerSG1 = df[(df['parameter'] == 'spr') & (df['device'] == 'Clinker SG 1')]['value'].values[0]
        sprClinkerSG2 = df[(df['parameter'] == 'spr') & (df['device'] == 'Clinker SG 2')]['value'].values[0]
        sprGypsum1 = df[(df['parameter'] == 'spr') & (df['device'] == 'Gypsum 1')]['value'].values[0]
        sprGypsum2 = df[(df['parameter'] == 'spr') & (df['device'] == 'Gypsum 2')]['value'].values[0]
        sprNDBlackstone = df[(df['parameter'] == 'spr') & (df['device'] == 'ND Blackstone')]['value'].values[0]
        sprFlyAsh = df[(df['parameter'] == 'spr') & (df['device'] == 'Fly ash')]['value'].values[0]
        sprMapei = df[(df['parameter'] == 'spr') & (df['device'] == 'Mapei')]['value'].values[0]
        spcTotal = df[(df['parameter'] == 'spc') & (df['device'] == 'Total')]['value'].values[0]
        SetPoint = {
            "sprClinkerVCM":sprClinkerVCM,
            "sprClinkerSG1":sprClinkerSG1,
            "sprClinkerSG2":sprClinkerSG2,
            "sprGypsum1":sprGypsum1,
            "sprGypsum2":sprGypsum2,
            "sprNDBlackstone":sprNDBlackstone,
            "sprFlyAsh":sprFlyAsh,
            "sprMapei":sprMapei,
            "spcTotal":spcTotal
        }
    except:
        SetPoint = {}
    return SetPoint
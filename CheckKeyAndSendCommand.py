import paho.mqtt.publish as publish
import smtplib, ssl
import secrets
import pandas as pd
import datetime
import json
from influxdb import DataFrameClient
dbclient = DataFrameClient(host='nangluong.iotmind.vn', port=8086, username='root', password='root', database='SCG')
dbclient.create_database('SCG')

def CheckKeyAndSendCommand(Email, InputCommitKey, NewSetPoint):
    try:
        RightCommitKey = dbclient.query('select last(*) from CommitKey where email=$Email and time > now() - 1m', bind_params={"Email":Email})["CommitKey"]["last_CommitKey"].values[0]
        if RightCommitKey == InputCommitKey:
            SendCommand(NewSetPoint)
            Time = datetime.datetime.utcnow()
            Name = dbclient.query('select last(*) from EmailsList where email=$Email', bind_params={"Email":Email})["EmailsList"]["last_name"].values[0]
            NewSetPoint.pop("inputKey", None)
            strNewSetPoint = json.dumps(NewSetPoint).replace('"', '').replace(': ', '= ').replace('{', '').replace('}', '')
            log = pd.DataFrame([{"time":Time, "name":Name, "email":Email, "NewSetPoints":strNewSetPoint}]).set_index('time')
            dbclient.write_points(log, 'LogsOfSetPoints', field_columns=['NewSetPoints'])  
            result = "Command has been sent"
        else:
            result = "Wrong verification code"
    except:
        result = "Verification code has expired or an incorrect email entered"
    return result
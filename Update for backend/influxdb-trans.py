import datetime
from influxdb import DataFrameClient

dbclient = DataFrameClient(host='localhost', port=8086, database='SCG')

def dif_per_hour():
    if len(dbclient.query('select count(diff_per_hour_value) from TimeseriesData').keys()) == 0:
        upper = dbclient.query('select * from TimeseriesData order by time desc limit 1')['TimeseriesData'].index[0].strftime('%Y-%m-%dT%H:%M:%SZ')
        dbclient.query('select last(realtime_value) - first(realtime_value) + max(realtime_value)*((max(realtime_value)-last(realtime_value))/(max(realtime_value)-last(realtime_value))) as diff_per_hour_value \
                        into TimeseriesData \
                        from TimeseriesData \
                        where parameter =~ /sv/ AND entity =~ /feeder/ AND time <= $upper \
                        group by time(1h),*', bind_params={'upper':upper})
    else:
        lower = dbclient.query('select diff_per_hour_value from TimeseriesData order by time desc limit 1')['TimeseriesData'].index[0].strftime('%Y-%m-%dT%H:%M:%SZ')
        upper = dbclient.query('select realtime_value from TimeseriesData order by time desc limit 1')['TimeseriesData'].index[0].strftime('%Y-%m-%dT%H:%M:%SZ')
        dbclient.query('select last(realtime_value) - first(realtime_value) + max(realtime_value)*((max(realtime_value)-last(realtime_value))/(max(realtime_value)-last(realtime_value))) as diff_per_hour_value \
                        into TimeseriesData \
                        from TimeseriesData \
                        where parameter =~ /sv/ AND entity =~ /feeder/ AND time >= $lower AND time <= $upper \
                        group by time(1h),*', bind_params={'lower':lower, 'upper':upper})

import time
while True:
    dif_per_hour()
    time.sleep(5)
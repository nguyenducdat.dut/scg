import paho.mqtt.client as mqtt

def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe("MT_PLANT/ECU01/DATA")

def on_message(client, userdata, msg):
    client.publish("MT_PLANT/ECU01/LINE01/DATA", msg.payload)
   
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.username_pw_set("mind", "123")
client.connect("nangluong.iotmind.vn", 16766, 60)

client.loop_forever()
import paho.mqtt.client as mqtt
import json
import datetime
import pandas
from influxdb import DataFrameClient

dbclient = DataFrameClient(host='localhost', port=8086, database='SCG')
dbclient.create_database('SCG')

def readDictLabel():
    f = open("dictLabel.txt", "r")
    dictLabel = json.loads(f.read())
    f.close()
    return dictLabel

def label_the_entity(dfData, strFactory, strGateway, strLine):
    dfLabel = dfData[["entity"]].copy()
    dfLabel.rename(columns={"entity": "entity_label"}, inplace=True)
    dfLabel.replace({'entity_label':dictLabel[strFactory][strGateway][strLine]}, inplace=True)
    dfData = pandas.concat([dfData, dfLabel], axis=1)
    return dfData

def calShift(time):
    hour = time.hour
    if hour >= 0 and hour < 8:
        return 'SHIFT 1'
    if hour >= 8 and hour < 16:
        return 'SHIFT 2'
    if hour >= 16 and hour <=23:
        return 'SHIFT 3'

def calCementType(dfData):
    dfSPR = dfData[dfData["parameter"] == "spr"]
    floatSumClinkerSPR = dfSPR[dfSPR["entity_label"].str.contains('Clinker', regex=True, na=False)].realtime_value.sum()
    if floatSumClinkerSPR > 94 and floatSumClinkerSPR < 96 :
        strCement_type = "PC40/50"
    elif floatSumClinkerSPR > 81 and floatSumClinkerSPR < 83 :
        strCement_type = "PCB40CLC"
    elif floatSumClinkerSPR > 69.0 and floatSumClinkerSPR < 71.0 :
        strCement_type = "SUPER WALL"
    else :
        strCement_type = "no name" 
    return strCement_type

def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe("MT_PLANT/ECU01/LINE01/DATA")

def on_message(client, userdata, msg):
    #GET payload
    jsonPayload = json.loads(msg.payload)
    
    #GET time
    strTime = jsonPayload["ts"]
    dtTime = datetime.datetime.strptime(strTime, "%Y-%m-%dT%H:%M:%S%z")
    
    #GET data
    jsonRawData = jsonPayload["d"]
    dfRawData = pandas.DataFrame(jsonRawData)
    dfData = pandas.DataFrame()
    dfData["realtime_value"] = dfRawData.value
    dfData[["entity", "parameter"]] = dfRawData.tag.str.split("\.\.", expand=True)

    #ADD Total of feeder MT_PLANT_LINE 02
    floatSumSPR = dfData[(dfData["parameter"] == "spr") & (dfData["entity"] != "powder_additive") & (dfData["entity"] != "1WP02")].realtime_value.sum()
    floatSumAR = dfData[(dfData["parameter"] == "ar") & (dfData["entity"] != "powder_additive") & (dfData["entity"] != "1WP02")].realtime_value.sum()
    floatSumAC = dfData[(dfData["parameter"] == "ac") & (dfData["entity"] != "powder_additive") & (dfData["entity"] != "1WP02")].realtime_value.sum()
    floatSumSV = dfData[(dfData["parameter"] == "sv") & (dfData["entity"] != "powder_additive") & (dfData["entity"] != "1WP02")].realtime_value.sum()

    total_spr = {"entity":"feeder_all", "parameter":"spr", "realtime_value":floatSumSPR}
    total_ar = {"entity":"feeder_all", "parameter":"ar", "realtime_value":floatSumAR}
    total_ac = {"entity":"feeder_all", "parameter":"ac", "realtime_value":floatSumAC}
    total_sv = {"entity":"feeder_all", "parameter":"sv", "realtime_value":floatSumSV}
    
    dfData = dfData.append(total_spr, ignore_index=True)
    dfData = dfData.append(total_ar, ignore_index=True)
    dfData = dfData.append(total_ac, ignore_index=True)
    dfData = dfData.append(total_sv, ignore_index=True)

    #ADD factory, gateway and line TAGs
    strTopic = msg.topic
    listTopic = strTopic.split("/")
    strFactory = listTopic[0]
    strGateway = listTopic[1]
    strLine = listTopic[2]
    dfData["factory"] = strFactory
    dfData["gateway"] = strGateway
    dfData["line"] = strLine
    
    #ADD entity_label TAG
    dfData = label_the_entity(dfData, strFactory, strGateway, strLine)
    
    #ADD shift TAG
    dfData["shift"] = calShift(dtTime)
    
    #ADD cement_type TAG
    dfData["cement_type"] = calCementType(dfData)
    
    #DROP fv_raw records
    dfData.set_index("parameter", inplace=True)
    dfData.drop("fv_raw", axis=0, inplace=True)
    
    #ADD and SET time as index
    dfData["time"] = dtTime
    dfData.reset_index(inplace=True)
    dfData.set_index("time", inplace=True)
    dbclient.write_points(dfData, "TimeseriesData", field_columns=["realtime_value"])

dictLabel = readDictLabel()    
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.username_pw_set("mind", "123")
client.connect("nangluong.iotmind.vn", 16766, 60)

client.loop_forever()